import {createStackNavigator} from 'react-navigation-stack';
import Login from '../screens/login/Login';
import SignIn from '../screens/signIn/SignIn';

export default createStackNavigator(
  {
    Login: {
      screen: Login,
    },
    SignIn: {
      screen: SignIn,
    },
  },
  {
    initialRouteName: 'SignIn',
    defaultNavigationOptions: {
      header: () => null,
      cardStyle: {
        backgroundColor: '#ffffff',
      },
    },
  },
);
