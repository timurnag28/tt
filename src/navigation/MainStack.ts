import {createStackNavigator} from 'react-navigation-stack';
import Main from '../screens/main/Main';

export default createStackNavigator(
  {
    Main: {
      screen: Main,
      navigationOptions: {
        headerTitle: 'Пользователь',
      },
    },
  },
  {
    initialRouteName: 'Main',
    defaultNavigationOptions: {
      header: () => null,
      cardStyle: {
        backgroundColor: '#fff',
      },
    },
  },
);
