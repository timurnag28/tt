import {createStackNavigator} from 'react-navigation-stack';
import Loading from '../screens/loading/Loading';

export default createStackNavigator(
  {
    Loading: {
      screen: Loading,
    },
  },
  {
    initialRouteName: 'Loading',
    defaultNavigationOptions: {
      header: () => null,
      cardStyle: {
        backgroundColor: '#ffffff',
      },
    },
  },
);
