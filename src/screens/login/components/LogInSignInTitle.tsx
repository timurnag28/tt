import React, {PureComponent} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {RobotoSlabBold} from '../../../share/fonts';

interface LogInSignInTitleProps {
  registrationType?: boolean;
}

export default class LogInSignInTitle extends PureComponent<
  LogInSignInTitleProps
> {
  render() {
    const {registrationType} = this.props;
    return (
      <View style={styles.container}>
        <Text style={!registrationType ? styles.smallTitle : styles.bigTitle}>
          Вход
        </Text>
        <Text style={registrationType ? styles.smallTitle : styles.bigTitle}>
          Регистрация
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    marginTop: 34,
    alignItems: 'flex-end',
    paddingHorizontal: 32,
  },
  smallTitle: {
    fontFamily: RobotoSlabBold,
    fontSize: 29,
    color: '#252D76',
  },
  bigTitle: {
    fontFamily: RobotoSlabBold,
    fontSize: 15,
    color: '#AEAEAE',
  },
});
