import React from 'react';
import {
  StyleProp,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import {UbuntuRegular} from '../../../share/fonts';
import FastImage from 'react-native-fast-image';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import EvilIcons from 'react-native-vector-icons/EvilIcons';

export const Avatar = ({
  path,
  setImageFunction,
  deleteImageFunction,
}: {
  path: string;
  style?: StyleProp<ViewStyle>;
  setImageFunction: () => void;
  deleteImageFunction: () => void;
}) => {
  return (
    <TouchableOpacity
      activeOpacity={1}
      onPress={setImageFunction}
      style={styles.container}>
      <Text style={styles.title}>Фото</Text>
      {path ? (
        <FastImage
          source={{
            uri: path,
          }}
          style={styles.image}
        />
      ) : (
        <View style={styles.imageContainer}>
          <View style={styles.emptyImageContainer}>
            <FontAwesome5Icon name={'user'} size={20} color={'#E7E7E7'} />
          </View>
        </View>
      )}
      <TouchableOpacity onPress={deleteImageFunction} style={styles.deleteIcon}>
        <EvilIcons name={'close'} size={17} color={'#CA2121'} />
      </TouchableOpacity>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '100%',
    paddingLeft: 30,
    marginTop: 30,
  },
  title: {
    fontFamily: UbuntuRegular,
    fontSize: 14,
    color: 'rgba(37, 45, 118, 0.5)',
  },
  image: {
    borderRadius: 200,
    height: 98,
    width: 98,
    borderColor: '#252D76',
    borderWidth: 2,
    marginLeft: 60,
  },
  imageContainer: {
    borderRadius: 200,
    height: 100,
    width: 100,
    marginLeft: 60,
    backgroundColor: '#E7E7E7',
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyImageContainer: {
    width: 26,
    height: 26,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    backgroundColor: 'rgba(48, 102, 224, 0.37)',
  },
  deleteIcon: {
    backgroundColor: '#FFFFFF',
    elevation: 3,
    width: 23,
    height: 23,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'flex-start',
  },
});
