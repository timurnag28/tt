import React from 'react';
import {StyleSheet, View, Linking} from 'react-native';
import {NavigationProps} from '../../share/inrefaces';
import Header from '../../share/components/Header';
import LogInSignInTitle from '../login/components/LogInSignInTitle';
import {CustomInput} from '../login/components/CustomInput';
import store from '../../stores';
import {FooterElement} from '../login/components/FooterElement';
import {observer} from 'mobx-react';
import {Avatar} from './components/Avatar';

@observer
export default class SignIn extends React.Component<NavigationProps> {
  handleSignUp() {
    store.signInStore
      .handleSignUp()
      .then(() => this.props.navigation.navigate('Main'));
  }

  render() {
    const {
      isLoading,
      emailErrorMessage,
      email,
      changeRegLogin,
      passwordErrorMessage,
      password,
      changeRegPassword,
      onEmptyInputsFunction,
      setAvatar,
      deleteAvatar,
      avatar,
    } = store.signInStore;
    return (
      <View style={styles.container}>
        <Header />
        <LogInSignInTitle registrationType={true} />
        <Avatar
          path={avatar}
          setImageFunction={setAvatar}
          deleteImageFunction={deleteAvatar}
        />
        <View
          style={{
            marginTop: 15,
          }}>
          <CustomInput
            disableInputs={isLoading}
            errorMessage={emailErrorMessage}
            value={email}
            title={'Email'}
            onChangeText={email => changeRegLogin(email)}
          />
          <CustomInput
            disableInputs={isLoading}
            errorMessage={passwordErrorMessage}
            value={password}
            style={{marginTop: 16}}
            secureTextEntry={true}
            title={'Пароль'}
            onChangeText={password => changeRegPassword(password)}
          />
        </View>
        <FooterElement
          isLoading={isLoading}
          typeRegistration={true}
          onPressText={() =>
            Linking.openURL('https://vk.com/').catch(() => alert('Ошибка!'))
          }
          onPressButton={
            email !== '' && password !== ''
              ? this.handleSignUp
              : onEmptyInputsFunction
          }
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
});
