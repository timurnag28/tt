import loginStore from './LoginStore';
import signInStore from './SignInStore';
import mainStore from './MainStore';

const store = {
  loginStore,
  signInStore,
  mainStore,
};

export default store;
