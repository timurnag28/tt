import {logIn, SigIn} from './Auth';

export default class API {
  static auth = {
    login: (email: string, password: string) =>
      interceptor(logIn(email, password)),
    sign: (email: string, password: string) =>
      interceptor(SigIn(email, password)),
  };
}

export const interceptor = (method: Promise<Response>) => {
  return method
    .then(async response => {
      console.log('response ' + response);
      return response;
    })
    .catch(error => {
      switch (error.code) {
        case 'auth/invalid-email': {
          throw new Error('Неверный email!');
        }
        case 'auth/wrong-password': {
          throw new Error('Неверный пароль!');
        }
        case 'auth/user-not-found': {
          throw new Error('Вы не найдены');
        }
        case 'auth/weak-password': {
          throw new Error('Слабый пароль!');
        }
        case 'auth/email-already-in-use': {
          throw new Error('Вы уже есть!');
        }
        default: {
          throw new Error(
            'Неизвестная ошибка. Вы держитесь тут, всего доброго вам и хорошего настроения!',
          );
        }
      }
    });
};
