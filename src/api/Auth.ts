// @ts-ignore
import firebase, {UserCredential} from 'react-native-firebase';

export function logIn(email: string, password: string): UserCredential{
  return firebase.auth().signInWithEmailAndPassword(email, password);
}

export function SigIn(email: string, password: string): UserCredential{
  return firebase.auth().createUserWithEmailAndPassword(email, password);
}
